export class ProvAppareils {
    appareils=[
        {
        name: "Machine à laver",
        status: "éteint"
        },
        {
        name: "Ordinateur",
        status: "éteint"
        },
        {
        name: "Lave vaisselle",
        status: "éteint"
        }
    ];

    turnOn(i){
        this.appareils[i].status = "allumé";
    }

    turnOff(i){
        this.appareils[i].status = "éteint";
    }
}
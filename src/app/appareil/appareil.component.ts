import { ProvAppareils } from './../services/prov.appareils';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent{
  @Input()appareilName;
  @Input()appareilStatus;
  @Input()appareilIndex;

  constructor(private provAppareils: ProvAppareils ) {

  }

  ngOnInit() {
  }

  onChangeStatus(i){
    if (this.provAppareils.appareils[i].status === "allumé"){
      this.provAppareils.appareils[i].status = "éteint"
      return
    }
    else{
      this.provAppareils.appareils[i].status = "allumé"
    }
    

    
    
  }

}

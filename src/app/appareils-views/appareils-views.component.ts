import { Component, OnInit } from '@angular/core';
import { ProvAppareils } from '../services/prov.appareils';


@Component({
  selector: 'app-appareils-views',
  templateUrl: './appareils-views.component.html',
  styleUrls: ['./appareils-views.component.scss']
})
export class AppareilsViewsComponent implements OnInit {
  appareils: any = [];

  constructor(private provAppareils: ProvAppareils){

  }

  ngOnInit() {
    this.appareils = this.provAppareils.appareils;
  }  

}

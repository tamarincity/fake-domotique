import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppareilComponent } from './appareil/appareil.component';
import { AppareilsViewsComponent } from './appareils-views/appareils-views.component';
import { AuthComponent } from './auth/auth.component';

import { ProvAppareils } from './services/prov.appareils';

const appRoutes: Routes = [
  { path: "appareils", component: AppareilsViewsComponent },
  { path: "auth", component: AuthComponent },
  { path: "", component: AppareilsViewsComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    AppareilComponent,
    AppareilsViewsComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ProvAppareils],
  bootstrap: [AppComponent]
})
export class AppModule { }
